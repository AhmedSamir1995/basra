﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

    [ExecuteInEditMode]
public class FanLayout : LayoutGroup
{
    public Vector3 FanCenter;
    [Range(0,360)]
    public float direction;
    public float stepAngle;
    public float distance;
    public override void CalculateLayoutInputVertical()
    {
        Debug.DrawLine(FanCenter+rectTransform.position, new Vector3(Mathf.Cos(direction * Mathf.Deg2Rad), Mathf.Sin(direction * Mathf.Deg2Rad), rectTransform.position.z) * distance + rectTransform.position);
        if (rectChildren.Count == 0)
            return;
        float startAngle = direction - stepAngle * (rectChildren.Count-1) / 2.0f;
        Debug.Log(rectChildren.Count / 2.0f);
        Debug.DrawLine(FanCenter + rectTransform.position, new Vector3(Mathf.Cos(startAngle * Mathf.Deg2Rad), Mathf.Sin(startAngle * Mathf.Deg2Rad), rectTransform.position.z) * distance + rectTransform.position, Color.red);
        for (int i=0;i<rectChildren.Count;i++)
        {
            float currentDirection=startAngle+stepAngle*i;
            //currentDirection = currentDirection * Mathf.Deg2Rad;
            rectChildren[i].rotation = Quaternion.Euler(Vector3.forward * (currentDirection + 90 + 180));
            rectChildren[i].position = -new Vector3(Mathf.Cos(direction * Mathf.Deg2Rad), Mathf.Sin(direction * Mathf.Deg2Rad), rectTransform.position.z) * distance+
                FanCenter + rectTransform.position + new Vector3(Mathf.Cos(currentDirection * Mathf.Deg2Rad), Mathf.Sin(currentDirection * Mathf.Deg2Rad), rectTransform.position.z)*distance;
        }
    }

    public override void SetLayoutHorizontal()
    {

    }

    public override void SetLayoutVertical()
    {

    }
}

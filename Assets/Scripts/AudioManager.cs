﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField] AudioClip cardDealAudioClip;
    [SerializeField] AudioClip cardFlipAudioClip;
    [SerializeField] AudioClip cardCollectAudioClip;
    [SerializeField] AudioClip winAudioClip;
    [SerializeField] AudioClip loseAudioClip;
    [SerializeField] AudioClip playersTurn;

    [SerializeField] AudioSource musicAudioSource;
    [SerializeField] AudioSource sfxAudioSource;

    public static AudioManager instance;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (instance == null)
            instance = this;
        else
            if (instance != this)
        {
            Destroy(gameObject);
            return;
        }
    }
    public void PlayCardDealAudioClip()
    {
        sfxAudioSource.PlayOneShot(cardDealAudioClip);
    }

    public void PlayCardFlipAudioClip()
    {
        sfxAudioSource.PlayOneShot(cardFlipAudioClip);
    }

    public void PlayCardCollectAudioClip()
    {
        sfxAudioSource.PlayOneShot(cardCollectAudioClip);
    }
    public void PLayWinAudioClip()
    {
        sfxAudioSource.PlayOneShot(winAudioClip);
    }
    
    public void PlayLoseAudioClip()
    {
        sfxAudioSource.PlayOneShot(loseAudioClip);
    }

    public void PlayPlayersTurn()
    {
        sfxAudioSource.PlayOneShot(playersTurn);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdPopUp : MonoBehaviour {
    [SerializeField]
    private string[] popupText;
    public UnityEngine.UI.Text popupTextbox;
    [SerializeField]
    private GameObject popupGameObject;
    private int textIndex=0;
    Animator popupAnimator;
    public UnityEngine.Events.UnityEvent onTimeOut;
    public float popupTimeOut;
    private void Start()
    {
        popupAnimator = popupGameObject.GetComponent<Animator>();
    }
    [ContextMenu("Show Popup")]
    public void ShowPopup()
    {
        if (popupTextbox && popupText.Length > 0)
        {
            textIndex++;
            textIndex %= popupText.Length;
            popupTextbox.text = popupText[textIndex];
        }
        if (popupAnimator)
        {
            ShowPopupAnimation();
            return;
        }
        popupGameObject.SetActive(true);
        Invoke("HidePopup", popupTimeOut);
    }
    public void ShowPopupAnimation()
    {
        popupAnimator.SetTrigger("ShowPopup");
    }
    public void HidePopup()
    {
        popupGameObject.SetActive(false);
        CallOnTimeOut();
    }
    public void CallOnTimeOut()
    {
        Debug.Log("Add Animation done");
        onTimeOut.Invoke();
    }
}

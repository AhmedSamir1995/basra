﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;
using GoogleMobileAds.Ump.Api;

public class AdMobManager : MonoBehaviour {

    public UnityEngine.UI.Text outputText;
    [SerializeField] GDPRStuff GDPRStuff;
    private BannerView bannerView;
    /// <summary>
    /// Interstitial but called regular for ease
    /// </summary>
    InterstitialAd regularAd;
    RewardedAd rewardBasedVideoAd;
    #region Android IDs
    [Header("Android IDs")]
    [SerializeField]
    private string AndroidAppID;
    [SerializeField]
    private string AndroidInterstitialAdID;
    [SerializeField]
    private string AndroidBannerID;
    [SerializeField]
    private string AndroidRewardedID;
    #endregion
    #region IOS IDs
    [Header("IOS IDs")]
    [SerializeField]
    private string IOSAppID;
    [SerializeField]
    private string IOSInterstitialAdID;
    [SerializeField]
    private string IOSBannerID;
    [SerializeField]
    private string IOSRewardedID;
    #endregion

    public static AdMobManager instance;
    public event System.Action OnRewardedAdReady;
    public event System.Action OnRewardedAdShown;
    public event System.Action<int> OnRewardedAdRewarded;
    #region actual IDs for current Platform
    private string appID;
    private string bannerID;
    private string regularID;
    private string rewardedID;
    #endregion
    /// <summary>
    /// A flag for determining whether to run in test mode, or not
    /// </summary>
    public bool test;

    [Header("Test IDs")]
    bool showRegular;
    [SerializeField]
    private string bannerIDTest;
    [SerializeField]
    private string regularIDTest;
    [SerializeField]
    private string rewardedIDTest;

    public int levelsPassed;
    public int levelsToShowAd;
    public AdPopUp popup;

    public UnityEngine.Events.UnityEvent<Vector2> onBannerAdShow;
    public float bannerHeight;
    // Use this for initialization
    void Start () {

        levelsPassed = 0;
        print("Admob start 75");
        DontDestroyOnLoad(gameObject);
        if (instance == null)
            instance = this;
        else
            if (instance != this)
        {
            Destroy(gameObject);
            return;
        }

#if UNITY_ANDROID
        appID =     AndroidAppID;
        bannerID =  AndroidBannerID;
        regularID = AndroidInterstitialAdID;
        rewardedID = AndroidRewardedID;
#elif UNITY_IOS
        appID =     IOSAppID;
        bannerID =  IOSBannerID;
        regularID = IOSInterstitialAdID;
        rewardedID = IOSRewardedID;
#endif

        print("Admob start 98");
        if (test)
        {
            levelsToShowAd = 2;
            bannerID = bannerIDTest;
            regularID = regularIDTest;
            rewardedID = rewardedIDTest;
        }
        print("Admob start 106");
        GDPRStuff = GetComponent<GDPRStuff>();
        GDPRStuff.onConsent += InitializeAds;
	}
    public void InitializeAds()
    {
        //MobileAds.Initialize(appID);

        MobileAds.Initialize(initStatus => {
            Print("Admob initialization: " + initStatus.ToString());
        });
        RequestRegularAd();
        //RequestRewardedAd();
        //RequestRewardedAd();
        //RequestBanner();

    }
    
    
    public void ShowBanner()
    {

        bannerView.Show();
        Print("Banner ad Show");
    }

    public void ShowAd()
    {
        if (regularAd != null && regularAd.CanShowAd())
        {
            Print("Showing interstitial ad.");
            regularAd.Show();
            //showRegular = false;
        }
        else
        {
            print("Interstitial ad is not ready yet.");
            //showRegular = true;
            levelsPassed = levelsToShowAd - 1;
        }

    }
    public void ShowRewardedAd()
    {
        Print("ShowRewarded");
        if (rewardBasedVideoAd != null && rewardBasedVideoAd.CanShowAd())
        {
            rewardBasedVideoAd.Show(RewardBasedVideoAd_OnAdRewarded);
        }

    }
    public IEnumerator ShowAdWhenReady()
    {
        return new WaitUntil(() => regularAd.CanShowAd());
    }

    public void RequestRegularAd()
    {
        if (regularAd != null)
        {
            regularAd.Destroy();
            regularAd = null;
        }

        //regularAd = new InterstitialAd(regularID);
        AdRequest request = new AdRequest();//.Builder().Build();

        InterstitialAd.Load(regularID, request, RegularAd_OnAdLoaded);
        
        Print("Regular ad requested");
    }





    private void RegularAd_OnAdOpening()
    {
        Print("Interstitial ad openning.");
    }

    private void RegularAd_OnAdLoaded(InterstitialAd ad, LoadAdError error)
    {
        // if error is not null, the load request failed.
        if (error != null || ad == null)
        {
            Debug.LogError("interstitial ad failed to load an ad " +
                           "with error : " + error.GetMessage());
            Invoke("RequestRegularAd", 5);
            return;
        }

        Debug.Log("Interstitial ad loaded with response : "
                  + ad.GetResponseInfo());

        regularAd = ad;


        //regularAd.onad += RegularAd_OnAdLoaded;
        regularAd.OnAdFullScreenContentOpened += RegularAd_OnAdOpening;
        regularAd.OnAdFullScreenContentFailed += RegularAd_OnAdFullScreenContentFailed;
        regularAd.OnAdFullScreenContentClosed += RegularAd_OnAdFullScreenContentClosed;

        Print("Interstitial ad loaded.");
        if(showRegular)
        {
            ShowAd();
        }
    }

    private void RegularAd_OnAdFullScreenContentClosed()
    {

        RequestRegularAd();
    }

    private void RegularAd_OnAdFullScreenContentFailed(AdError obj)
    {
        Print("ad failed. Inter" + obj.GetMessage());
        Invoke("RequestRegularAd", 5);
        //RequestRegularAd();
    }

    public void RequestBanner()
    {
        bannerView = new BannerView(bannerID, AdSize.Banner, AdPosition.Top);

        bannerView.OnBannerAdLoaded += BannerView_OnBannerAdLoaded; ;
        bannerView.OnAdFullScreenContentOpened += BannerView_OnAdFullScreenContentOpened; ;
        bannerView.OnBannerAdLoadFailed += BannerView_OnAdFailedToLoad;
        bannerView.OnAdFullScreenContentClosed += BannerView_OnAdFullScreenContentClosed; ;

        AdRequest request = new AdRequest();
        bannerView.LoadAd(request);
        //Print("Banner ad requested");
    }

    private void BannerView_OnAdFullScreenContentOpened()
    {

        Print("Banner ad opened.");
    }

    private void BannerView_OnBannerAdLoaded()
    {
        Print($"Banner ad loaded. w:{AdSize.Banner.Width}, h:{AdSize.Banner.Height}");

        bannerView.GetHeightInPixels();
        //GoogleMobileAds.Api.BannerView.GetHeightInPixels()
        onBannerAdShow.Invoke(new Vector2(bannerView.GetWidthInPixels(), bannerView.GetHeightInPixels()));
        bannerHeight = bannerView.GetWidthInPixels();
        bannerView.Show();
        //bannerView.Show();
    }

    private void BannerView_OnAdFullScreenContentClosed()
    {
        onBannerAdShow.Invoke(new Vector2(0, 0));
        bannerHeight = 0;
        RequestBanner();
    }

    private void BannerView_OnAdFailedToLoad(LoadAdError loadAdError)
    {
        onBannerAdShow.Invoke(new Vector2(0, 0));
        bannerHeight = 0;
        Print("Banner ad Failed. Error: " + loadAdError.GetMessage());
        Invoke("RequestBanner", 5);
        //RequestBanner();
    }


    public void RequestRewardedAd()
    {
        if (rewardBasedVideoAd != null)
        {
            rewardBasedVideoAd.Destroy();
            rewardBasedVideoAd = null;
        }
        //if(rewardBasedVideoAd==null)
        //{
        Debug.Log("RewardAd Requested ID: " + rewardedID);
        //rewardBasedVideoAd = new RewardedAd(rewardedID);// RewardBasedVideoAd.Instance;
        AdRequest request = new AdRequest();

        RewardedAd.Load(rewardedID, request, RewardBasedVideoAd_OnAdLoaded);


        // Load the rewarded video ad with the request.
        //rewardBasedVideoAd.LoadAd(request);
        rewardBasedVideoAd.OnAdFullScreenContentClosed += RewardBasedVideoAd_OnAdClosed;
        //rewardBasedVideoAd.OnAdCompleted += RewardBasedVideoAd_OnAdCompleted;
        rewardBasedVideoAd.OnAdFullScreenContentFailed += RewardBasedVideoAd_OnAdFailedToLoad;
        //rewardBasedVideoAd.OnAdLeavingApplication += RewardBasedVideoAd_OnAdLeavingApplication;
        rewardBasedVideoAd.OnAdFullScreenContentOpened += RewardBasedVideoAd_OnAdFullScreenContentOpened;
        
        //rewardBasedVideoAd.OnAdStarted += RewardBasedVideoAd_OnAdStarted;
        //}

        Print("Rewarded ad requested");
    }

    private void RewardBasedVideoAd_OnAdFullScreenContentOpened()
    {
        Print("Rewarded Ad Opening");
        OnRewardedAdShown?.Invoke();
    }

    private void RewardBasedVideoAd_OnAdStarted(object sender, System.EventArgs e)
    {
        Print("Rewarded Ad OnAdStarted");
    }

    private void RewardBasedVideoAd_OnAdRewarded(Reward reward)
    {
        const string rewardMsg =
        "Rewarded ad rewarded the user. Type: {0}, amount: {1}.";
        Debug.Log(String.Format(rewardMsg, reward.Type, reward.Amount));

        Print("Rewarded Ad On Ad Rewarded" + reward.Amount + " of " + reward.Type);
        OnRewardedAdRewarded?.Invoke((int)reward.Amount);
    }

    private void RewardBasedVideoAd_OnAdLoaded(RewardedAd ad, LoadAdError error)
    {
        // if error is not null, the load request failed.
        if (error != null || ad == null)
        {
            Print("Rewarded ad failed to load an ad " +
                           "with error : " + error);
            return;
        }

        Print("Rewarded ad loaded with response : "
                  + ad.GetResponseInfo());

        rewardBasedVideoAd = ad;
        //Print("Rewarded Ad OnLoaded");
        OnRewardedAdReady?.Invoke();
    }

    private void RewardBasedVideoAd_OnAdLeavingApplication(object sender, System.EventArgs e)
    {
        Print("Rewarded Ad On Leaving App");
    }

    private void RewardBasedVideoAd_OnAdFailedToLoad(AdError e)
    {
        Print("Rewarded Ad OnFailedToLoad" + e.GetMessage());

        Invoke("RequestRewardedAd", 3);
        //RequestRewardedAd();
    }

    private void RewardBasedVideoAd_OnAdCompleted(object sender, System.EventArgs e)
    {

        Print("Rewarded Ad OnCompleted");
    }

    private void RewardBasedVideoAd_OnAdClosed()
    {
        RequestRewardedAd();
        Print("Rewarded Ad Closed");
    }

    public void Print(object message)
    {
        Debug.Log(message);
        if (outputText)
        {
            //Print(message);
            outputText.text += "\n";
            outputText.text += message.ToString();
        }
    }

    public void LevelPassed()
    {
        levelsPassed += 1;
        if(levelsPassed>=levelsToShowAd)
        {
            levelsPassed = 0;
            if (popup)
                popup.ShowPopup();
            else
            {
                ShowAd();
                //if (UnityEngine.Random.Range(1, 10) < 7)
                //    ShowAd();
                //else
                //    ShowRewardedAd();
            }
        }
    }
    private void OnDestroy()
    {
        if (bannerView != null)
            bannerView.Destroy();
        if (regularAd != null)
            regularAd.Destroy();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardedAdButton : MonoBehaviour
{
    public void ShowRewardedAd()
    {
        AdMobManager.instance.ShowRewardedAd();
    }

    private void Start()
    {
        if(!AdMobManager.instance)
        {
            Invoke("Start", 1);
            return;
        }
        AdMobManager.instance.OnRewardedAdReady += Instance_OnRewardedAdReady;
        AdMobManager.instance.OnRewardedAdRewarded += Instance_OnRewardedAdRewarded;
        AdMobManager.instance.OnRewardedAdShown += Instance_OnRewardedAdShown;
        Debug.Log("RewardButtonStart");
    }

    private void Instance_OnRewardedAdShown()
    {
        gameObject.SetActive(false);
        Debug.Log("RewardedButtonAdShown");
    }

    private void Instance_OnRewardedAdRewarded(int obj)
    {
        //GameManager.totalCoins += obj;
        //StoreManager.instance.UpdateCoinsText();
        Debug.Log("RewardedButtonRewarded10");
    }

    private void Instance_OnRewardedAdReady()
    {
        gameObject.SetActive(true);
        Debug.Log("RewardedButtonAdReady");
    }
}

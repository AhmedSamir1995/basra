﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Ump;
using GoogleMobileAds.Ump.Api;
public class GDPRStuff : MonoBehaviour {
    #region old stuff
    //   [SerializeField]
    //   private string linkGDPR;

    //   // Use this for initialization
    //   void Start () {
    //   }

    //// Update is called once per frame
    //void Update () {

    //}
    //   public void OpenTermsAndConditionsLink()
    //   {
    //       Application.OpenURL(linkGDPR);
    //   }
    #endregion

    public event System.Action onConsent;
    ConsentForm _consentForm;
    // Start is called before the first frame update
    void Start()
    {
        //var debugSettings = new ConsentDebugSettings
        //{
        //    // Geography appears as in EEA for debug devices.
        //    DebugGeography = DebugGeography.EEA,
        //    TestDeviceHashedIds = new List<string>
        //    {
        //        "ECD8EB6276F7B43A0B1995A10993F716"
        //    }
        //};
        // Here false means users are not under age.
        ConsentRequestParameters request = new ConsentRequestParameters
        {
            TagForUnderAgeOfConsent = false,
            //ConsentDebugSettings = debugSettings,
        };
        print($"Requesting Conscent");

        // Check the current consent information status.
        ConsentInformation.Update(request, OnConsentInfoUpdated);
        //onConsent = LoadConsentForm;
    }

    void OnConsentInfoUpdated(FormError error)
    {
        if (error != null)
        {
            print("Admob consent error");
            // Handle the error.
            UnityEngine.Debug.LogError(error);
            return;
        }
        print("Consent info received");
            print("consent status: " + ConsentInformation.ConsentStatus);
        if (ConsentInformation.ConsentStatus != ConsentStatus.Required)
            onConsent?.Invoke();
        else
        if (ConsentInformation.IsConsentFormAvailable())
        {
            LoadConsentForm();
        }
        else
            StartCoroutine("WaitAndLoadConsentForm");
        // If the error is null, the consent information state was updated.
        // You are now ready to check if a form is available.

    }
    IEnumerator WaitAndLoadConsentForm()
    {
        print("Waiting till form is ready");
        yield return new WaitUntil(() => ConsentInformation.IsConsentFormAvailable());
        LoadConsentForm();
    }
    void LoadConsentForm()
    {
        print("Loading consent form");
        // Loads a consent form.
        ConsentForm.Load(OnLoadConsentForm);
    }

    void OnLoadConsentForm(ConsentForm consentForm, FormError error)
    {
        print($"Consent form loaded with {ConsentInformation.ConsentStatus} status");
        if (error != null)
        {
            // Handle the error.
            UnityEngine.Debug.LogError(error);
            return;
        }

        // The consent form was loaded.
        // Save the consent form for future requests.
        _consentForm = consentForm;
        // You are now ready to show the form.
        if (ConsentInformation.ConsentStatus == ConsentStatus.Required)
        {
            print("Consent required, will show form");
            _consentForm.Show(OnShowForm);
            
        }
        else
            if(ConsentInformation.ConsentStatus == ConsentStatus.Obtained ||
            ConsentInformation.ConsentStatus == ConsentStatus.NotRequired ||
            ConsentInformation.ConsentStatus == ConsentStatus.Unknown)
        {
            print("Consent not required");
            onConsent?.Invoke();
        }
    }


    void OnShowForm(FormError error)
    {
        if (error != null)
        {
            // Handle the error.
            UnityEngine.Debug.LogError(error);
            return;
        }

        // Handle dismissal by reloading form.
        LoadConsentForm();
    }

}

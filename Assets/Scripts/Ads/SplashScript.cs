﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScript : MonoBehaviour
{
    [SerializeField]
    private string linkGDPR;
	Animator fader;
    public GameObject popup_GDPR;
	void Start ()
	{
		fader = GameObject.Find ("FaderNew").GetComponent<Animator> ();

        //PlayerPrefs.SetInt("AcceptedGDPR", 0);
        //PlayerPrefs.Save();
        if (PlayerPrefs.GetInt("AcceptedGDPR")==1)
            LoadHome();
        else
        {
            ShowGDPRPopup();
        }
	}
    public void LoadHome()
    {
        PlayerPrefs.SetInt("AcceptedGDPR", 1);
        PlayerPrefs.Save();
        StartCoroutine (changeScene ());
    }
	IEnumerator changeScene ()
	{
		yield return new WaitForSeconds (1f);
		fader.Play ("FadeOut");
		yield return new WaitForSeconds (1f);
		SceneManager.LoadScene (1);
	}
    public void Quit()
    {
        Application.Quit();
    }

    public void ShowGDPRPopup()
    {
        popup_GDPR.SetActive(true);
    }

    public void OpenTermsAndConditionsLink()
    {
        Application.OpenURL(linkGDPR);
    }
}

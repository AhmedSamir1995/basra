﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;
public class UnityRewardedAd : MonoBehaviour//, IUnityAdsLoadListener, IUnityAdsShowListener
{
//    #region Data
//    [SerializeField] UnityEngine.UI.Button showAdButton;
//    [SerializeField] string adUnitIDAndroid = "Rewarded_Android";
//    [SerializeField] string adUnitIDIos = "Rewarded_iOS";
//    string adUnitID;
//    #endregion

//    #region Loading

//    private void Awake()
//    {
//#if UNITY_IOS
//        adUnitID = adUnitIDIos;
//#elif UNITY_ANDROID
//        adUnitID = adUnitIDAndroid;
//#endif

//        showAdButton.interactable = false;
//    }

//    public void LoadAd()
//    {

//        Debug.Log("Loading Ad " + adUnitID);
//        Advertisement.Load(adUnitID, this);
//    }

//    //Load listener interface implementation
//    public void OnUnityAdsAdLoaded(string placementId)
//    {
//        Debug.Log($"Unity ad {placementId} Loaded Successfully!");
//        if (placementId.Equals(adUnitID))
//        {

//            showAdButton.interactable = true;
//            showAdButton.onClick.AddListener(ShowAd);
//        }
//    }

//    public void OnUnityAdsFailedToLoad(string placementId, UnityAdsLoadError error, string message)
//    {
//        Debug.Log($"Unity Ad {placementId} Load failed with error: {error.ToString()} - {message}");
//    }
//    #endregion


//    #region Showing

//    public void ShowAd()
//    {
//        Debug.Log("Showing Ad " + adUnitID);
//        showAdButton.interactable = false;
//        Advertisement.Show(adUnitID, this);
//    }
//    // Show Listener Interface implementation
//    public void OnUnityAdsShowFailure(string placementId, UnityAdsShowError error, string message)
//    {
//        Debug.Log($"Unity Ad {placementId} Show failed with error: {error.ToString()} - {message}");
//    }

//    public void OnUnityAdsShowStart(string placementId)
//    {
//        Debug.Log($"UnityAd {placementId} Show started!");
//    }
//    public void OnUnityAdsShowClick(string placementId)
//    {
//        Debug.Log($"Unity Ad {placementId} clicked!");
//    }

//    public void OnUnityAdsShowComplete(string placementId, UnityAdsShowCompletionState showCompletionState)
//    {
//        if (placementId.Equals(adUnitID))
//        {
//            Debug.Log($"{placementId} - {showCompletionState}");
//            switch (showCompletionState)
//            {
//                case UnityAdsShowCompletionState.SKIPPED:
//                    break;
//                case UnityAdsShowCompletionState.COMPLETED:
//                    //Grant Reward
//                    break;
//                case UnityAdsShowCompletionState.UNKNOWN:
//                    break;
//            }
//        }
//        LoadAd();
//    }
//    #endregion
}

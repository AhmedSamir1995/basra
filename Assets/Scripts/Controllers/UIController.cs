﻿using UnityEngine;

using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

public class UIController : EventReceiverBehaviour {
    public GameObject panelStart,panelEnd;
    public GameObject[] winLoseText;
    public void StartGame()
    {
        panelStart.SetActive(false);
        GameController.instance.StartGame();
    }

    public override void OnPause(bool openMenu)
    {
        
    }

    public override void OnLevelComplete(int winner)
    {
        base.OnLevelComplete(winner);
        panelEnd.SetActive(true);
        if (winner == 0)
        {
            winLoseText[0].SetActive(true);
            AudioManager.instance.PLayWinAudioClip();
        }
        else
        {
            winLoseText[1].SetActive(true);
            AudioManager.instance.PlayLoseAudioClip();
        }
    }
    public void ReloadLevel()
    {
       SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void LoadLevel(int i)
    {
        SceneManager.LoadScene(i);
    }

    public void OnQuit()
    {
        //System.Diagnostics.Process.GetCurrentProcess().Kill();
        Application.Quit();
    }
}

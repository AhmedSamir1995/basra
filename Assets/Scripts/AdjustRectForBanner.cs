using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdjustRectForBanner : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    RectTransform rectTransform;
    [SerializeField]
    float top = 320;
    AdMobManager admobManager;
    //public float top = -320;
    [ContextMenu("Update offsets")]
    void UpdateOffsets()
    {
        rectTransform.offsetMin = new Vector2(rectTransform.offsetMin.x, rectTransform.offsetMin.y);
        //rectTransform.offsetMax = new Vector2(rectTransform.offsetMax.x, top * (Screen.dpi / 140f));
        rectTransform.offsetMax = new Vector2(rectTransform.offsetMax.x, -top );
        print($"screen dpi: {Screen.dpi}, top: {top}");
    }

    private void Start()
    {
        if (AdMobManager.instance)
            admobManager = AdMobManager.instance;
        else
            admobManager = FindObjectOfType<AdMobManager>();
        admobManager.onBannerAdShow.AddListener(UpdateTop);
        print("adjust");
        top = admobManager.bannerHeight > 0 ? 220 : 0;
        UpdateOffsets();
    }
    void UpdateTop(Vector2 vector2)
    {
        top = vector2.y > 0 ? 220 : 0;
        UpdateOffsets();
    }
    private void OnDestroy()
    {
        AdMobManager.instance.onBannerAdShow.RemoveListener(UpdateTop);
    }
}

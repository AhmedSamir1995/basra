﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsManager : MonoBehaviour
{
    public Settings settings;

    public void SettingsInit()
    {

    }


    public class Settings
    {
        private static Settings settings;
        private int soundVolume;//100%
        private int sfxVolume;  //100%
        private ScreenOrientation screenOrientation;
        private float AIDelay;
        private Color BackgroundColor;
        //private Level Of Hardness;
        private Language language;
        private bool Hint;
        private bool vibration;

        Settings()
        {
            settings = new Settings();

        }


        private void ReadSettingsFile()
        {
            soundVolume = PlayerPrefs.GetInt("Sound_Volume", 50);
            sfxVolume = PlayerPrefs.GetInt("SFX_Volume", 50);
            screenOrientation = (ScreenOrientation)PlayerPrefs.GetInt("Screen_Orientation", 3);
            AIDelay = PlayerPrefs.GetFloat("AI_Delay", 1.5f);
            BackgroundColor = new Color(PlayerPrefs.GetFloat("BG_R"), PlayerPrefs.GetFloat("BG_G"), PlayerPrefs.GetFloat("BG_B"));
        
            //private Level Of Hardness;
            language = (Language)PlayerPrefs.GetInt("Language",0);
            Hint= PlayerPrefs.GetInt("Hint",1)==0?false:true;
            vibration = PlayerPrefs.GetInt("Vibration", 1) == 0 ? false : true;
    
        }

        private void SaveSettings()
        {
            PlayerPrefs.SetInt("Sound_Volume", soundVolume);
            PlayerPrefs.SetInt("SFX_Volume", sfxVolume);
            PlayerPrefs.SetInt("Screen_Orientation", (int)screenOrientation);
            PlayerPrefs.SetFloat("AI_Delay", AIDelay);
            //Background Color R,G,B
            PlayerPrefs.SetFloat("BG_R", BackgroundColor.r);
            PlayerPrefs.SetFloat("BG_G", BackgroundColor.g);
            PlayerPrefs.SetFloat("BG_B", BackgroundColor.b);
            //private Level Of Hardness;
            PlayerPrefs.SetInt("Language", (int)language);
            PlayerPrefs.SetInt("Hint", Hint ? 1 : 0);
            PlayerPrefs.SetInt("Vibration", vibration ? 1 : 0);
            PlayerPrefs.Save();
        }

        public enum Language
        {
            English,
            Arabic,
            French,
            German
        }
    }
}


﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class Game : EventReceiverBehaviour
{
    [SerializeField]
    private Image _ThrownCards;
    [SerializeField]
    private GameObject DeckCard;
    [SerializeField]
    public Deck _deck;
    [SerializeField]
    private PlayerBase[] _players;
    [SerializeField]
    [Multiline]
    string preDeskScoreText;
    [SerializeField]
    private Text _textDeskScore;
    [Range(0.1f, 3f)]
    [SerializeField]
    private float playedCardShowTime = 0.5f;
    //[SerializeField]
    //private Player _player;
    //[SerializeField]
    //private int _numberOfPlayers = 2;
    //[SerializeField]
    //private PlayerAI[] _playersAI;
    public Sprite[] Cards;
    private int _playerOrder = 1;
    private List<Card> _thrownDeck = new List<Card>();
    private List<Image> TableCards = new List<Image>();
    private Card _lastThrownCard = null;
    private Card _lastThrownCardByPlayer;
    private int _currentScore;

    //  private int whoseTurn;


    void Start()
    {
        _playerOrder = 1;
        Cards = Resources.LoadAll<Sprite>("cardSprite");
        //  Setup();
    }


    private int PlayerOrder
    {
        get { return _playerOrder; }
        set
        {
            _playerOrder = value;

        }
    }
    private int CurrentScore
    {
        get { return _currentScore; }
        set
        {
            _currentScore = value;
            _textDeskScore.text = preDeskScoreText + _currentScore;
        }
    }

    public override void OnStart()
    {
        base.OnStart();
        // start with 4 cards
        Setup();
    }

    void Setup()
    {
        for (int i = 0; i < 4; i++)
        {
            Card card = _deck.GetRandomCardForTable();
           
            _thrownDeck.Add(card);
            CurrentScore += card.number;
            _lastThrownCard = card;

            DrawSprites();
            Debug.Log("Setup " + _lastThrownCard.NumericName);
        }
        
        DealCards();

        AudioManager.instance.PlayCardDealAudioClip();
        AdMobManager.instance.ShowBanner();
    }

    public int getSpriteIndex(int suit, int value)
    {
        return ((suit - 1) * 13) + (value == 14 ? 0 : value - 1);//get relative card texture

    }
    public static void SortDeck(List<Card> _thrownDeck)
    {

        _thrownDeck.Sort((Card x, Card y) => y.number.CompareTo(x.number));
    }
    public void DrawSprites()
    {
        SortDeck(_thrownDeck);

        if (_lastThrownCardByPlayer != null)
        {
            _ThrownCards.sprite = Cards[getSpriteIndex(_lastThrownCardByPlayer.suit, _lastThrownCardByPlayer.number)];
            _ThrownCards.gameObject.SetActive(true);
        }
        else
        {
            _ThrownCards.gameObject.SetActive(false);
        }
        float initialPosition = -_thrownDeck.Count * (90.0f) / 2.0f;
        if (_thrownDeck.Count == 0)
        {
            // _ThrownCards = null;
            foreach (var item in TableCards)
            {
                item.gameObject.SetActive(false);
            }
            //_ThrownCards.transform.localPosition = Vector3.zero;
        }
        for (int i = 0; i < _thrownDeck.Count; i++)
        {
            if (TableCards.Count <= i)
            {
                Image CurrentCard = GameObject.Instantiate(DeckCard, _ThrownCards.transform.parent).GetComponent<Image>();
                CurrentCard.gameObject.SetActive(true);
                CurrentCard.sprite = Cards[getSpriteIndex(_thrownDeck[i].suit, _thrownDeck[i].number)];
                CurrentCard.transform.localPosition = new Vector3(initialPosition + i * 90.0f, 0);
                TableCards.Add(CurrentCard);
            }
            else
            {
                Image CurrentCard = TableCards[i];
                CurrentCard.sprite = Cards[getSpriteIndex(_thrownDeck[i].suit, _thrownDeck[i].number)];
                CurrentCard.transform.localPosition = new Vector3(initialPosition + i * 90.0f, 0);
                CurrentCard.gameObject.SetActive(true);
            }
            if (i == _thrownDeck.Count - 1)
            {
                for (int j = i + 1; j < TableCards.Count; j++)
                {
                    TableCards[j].gameObject.SetActive(false);
                }
            }
        }
    }
    private void DealCards()
    {
        if (_deck.NumberOfCards > 0)
        {
            Debug.Log("Dealing");
            for (int i = 0; i < _players.Length; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Card card = _deck.GetRandomCard();
                    _players[i].AddCard(j, card);
                }
            }
            GiveTurnToCurrentPlayer();
        }
        else
        {
            // game ended check score
            Debug.Log("cards finished");
            int score = 0;
            int winner = 0;
            for (int i = 0; i < _players.Length; i++)
            {
                if (_players[i].Score > score)
                {
                    score = _players[i].Score;
                    winner = i;
                }
            }
            GameController.instance.EndGame(winner);
        }
    }

    public void ThrowCard(Card card)
    {
        Debug.Log("card " + card.number + " suit " + card.suit);

        CurrentScore += card.number;
        // show them 


            AudioManager.instance.PlayCardFlipAudioClip();
        // check card
        if (_lastThrownCard == null)
        {
            Debug.Log("First card");
            _thrownDeck.Add(card);
            _lastThrownCard = card;
            CheckPlayersStatus();
            DrawSprites();
            return;
        }
        // we have thrown card
        if ((card.number == _lastThrownCard.number && _thrownDeck.Count == 1) ||
            (card.number == 7 && card.cardSuit == CardSuit.Diamonds))
        {
            //Basra & 7 Diamond basra
            // Pisti take it 
            Debug.Log("Basra");
            CurrentScore += 10;
            _lastThrownCardByPlayer = card;
            DrawSprites();
            StartCoroutine(TakeCardsForCurrentPlayer());
            return;

        }
        else if (card.number == Card.JACK)
        {
            // take it all
            if (_thrownDeck.Count > 0)
            {
                _lastThrownCardByPlayer = card;
                DrawSprites();
                StartCoroutine(TakeCardsForCurrentPlayer());
            }
            else
            {
                _thrownDeck.Add(card);
                _lastThrownCard = card;
                CheckPlayersStatus();
                DrawSprites();
            }
            return;
        }

        _lastThrownCard = card;
        _lastThrownCardByPlayer = card;
        DrawSprites();
        StartCoroutine(TakeCardsThatEqualPlayerCard());

    }

    IEnumerator TakeCardsForCurrentPlayer()
    {
        yield return new WaitForSeconds(playedCardShowTime);
        AudioManager.instance.PlayCardCollectAudioClip();
        Debug.Log("taking table cards for player " + PlayerOrder + " With total score of " + CurrentScore);
        // reset UI 
        _ThrownCards.sprite = null;
        _lastThrownCard = null;
        _thrownDeck = new List<Card>();
        //TODO give score to player
        AddScoreToPlayer();
        _lastThrownCardByPlayer = null;
        DrawSprites();
        // check status
        CheckPlayersStatus();
    }

    IEnumerator TakeCardsThatEqualPlayerCard()
    {
        yield return new WaitForSeconds(playedCardShowTime);
        Debug.Log("taking cards that suits " + _lastThrownCard.NumericName +" For Player " + PlayerOrder);

        int playerScore = 0;
        playerScore += hasSimilarCards(_lastThrownCard.number,_thrownDeck);// remove similar cards and calculate score
        if (_lastThrownCard.number <= 10 &&
            _lastThrownCard.number > 1) //Exclude Jack,Queen,King,Ace from Summition
        {
            playerScore += hasThatEquals(_lastThrownCard.number);
            if (_thrownDeck.Count == 0)
                playerScore += 10;
        }
       if (playerScore == 0)
            _thrownDeck.Add(_lastThrownCard);
        else
        {
            playerScore += _lastThrownCard.number;
            AudioManager.instance.PlayCardCollectAudioClip();
            _lastThrownCard = null;
            DrawSprites();
        }
        if (_thrownDeck.Count == 0)
            _lastThrownCard = null;
        else
            _lastThrownCard = _thrownDeck[_thrownDeck.Count - 1];

        //TODO give score to player
        print("Player "+PlayerOrder + " Scored " + playerScore);
        AddScoreToPlayer(playerScore);
        _lastThrownCardByPlayer = null;
        DrawSprites();
        // check status
        CheckPlayersStatus();

    }



    private void CheckPlayersStatus()
    {
        // change player order
        PlayerOrder++;
        if (PlayerOrder > _players.Length)
        {
            PlayerOrder = 1;
        }
        for (int i = 0; i < _players.Length; i++)
        {
            // if we have cards continue
            if (_players[i].CardCount > 0)
            {
                GiveTurnToCurrentPlayer();
                return;
            }
        }
        // else deal card
        DealCards();
    }

    private void GiveTurnToCurrentPlayer()
    {

        _players[PlayerOrder - 1].IsMyTurn = true;
        Debug.Log("PlayerOrder " + PlayerOrder);

    }
    private void AddScoreToPlayer()
    {
        _players[PlayerOrder - 1].Score += CurrentScore;
        CurrentScore = 0;

    }
    private void AddScoreToPlayer(int Score)
    {
        _players[PlayerOrder - 1].Score += Score;
        CurrentScore -= Score;

    }
    private void getPlayerOrder()
    {

    }
    
    int hasThatEquals(int number)
    {
        return hasThatEquals(number, _thrownDeck);
    }
    public static int hasThatEquals(int number, int currentIndex, List<Card> _thrownDeck)
    {
        SortDeck(_thrownDeck);
        if (currentIndex < 0 || currentIndex >= _thrownDeck.Count)//Check That current index is within index Range
        {
            return 0;
        }
        if (_thrownDeck[currentIndex].number > number)//if the number I'm looking for is less than this check the next element
            if (currentIndex < _thrownDeck.Count - 1)
                return hasThatEquals(number, currentIndex + 1, _thrownDeck);
            else
            {
                return 0;
            }
        int temp = 0;

        if (number==_thrownDeck[currentIndex].number)//if this is the number I'm looking for remove it from the list and return it
        {
            _thrownDeck.RemoveAt(currentIndex);
            return number;
        }
        //if its not what I'm looking for but might sum with something to make it then find this something
        if (currentIndex < _thrownDeck.Count - 1)
        {
            temp = hasThatEquals(number - _thrownDeck[currentIndex].number, currentIndex + 1, _thrownDeck);
            if (temp != 0)
            {
                temp += _thrownDeck[currentIndex].number;
                _thrownDeck.RemoveAt(currentIndex);
                return temp;
            }
            else
            {
                return hasThatEquals(number, currentIndex + 1, _thrownDeck);
            }
        }
        else
            return 0;

    }

    public static int hasThatEquals(int number, List<Card> _thrownDeck)
    {
        SortDeck(_thrownDeck);
        int NewScore;
        int Score = 0;
        do
        {
            NewScore = hasThatEquals(number, 0, _thrownDeck);
            Score += NewScore;
        }while (NewScore != 0) ;
        return Score;
    }
    public static int hasSimilarCards(int number, List<Card> _thrownDeck)
    {
        SortDeck(_thrownDeck);
        int playerScore=0;
        int iterationScore=playerScore;
        do
        {
            iterationScore = hasCard(number, _thrownDeck);
            playerScore += iterationScore;
        } while (iterationScore != 0);
        return playerScore;
    }
    public static int hasCard(int number, List<Card> _thrownDeck)
    {

        if (_thrownDeck.Exists((Card x) => x.number == number))
        {

            _thrownDeck.Remove(_thrownDeck.Find((Card x) => x.number == number));
            return number;

        }
        return 0;

    }


    public List<Card> GetDeckCards()
    {
        SortDeck(_thrownDeck);
        List<Card> Deck = new List<Card>();
        foreach(var item in _thrownDeck)
        {
            Deck.Add(item);
        }
        return Deck;
    }
    public int GetCurrentScore
    {
        get
        {
            return CurrentScore;
        }
    }
}

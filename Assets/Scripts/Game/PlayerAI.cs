﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
public class PlayerAI : PlayerBase
{
    public float delay=.1f;

    public override bool IsMyTurn
    {
        get
        {
            return _isMyTurn;
        }
        set
        {
            _isMyTurn = value;
            if (_isMyTurn)
            {
                // TODO do a proper AI here without Linq!!
                //Play the card that gets Highest Score
                
                List<Card> TableDeck;
                //int[] HandCardScores = new int[mDeck.Count];
                int HandCardScore=0;
                int highestScoreIndex = 0;
                int HighestScore = 0;

                for (int i = 0; i < mDeck.Count; i++)
                {
                    if (mDeck.ElementAt(i).Value.number == Card.JACK ||
                        mDeck.ElementAt(i).Value.number == 7 && mDeck.ElementAt(i).Value.cardSuit == CardSuit.Diamonds)
                    {
                        if (mGame.GetDeckCards().Count != 0) //if AI has Jack or 7 Diamond and the Table isn't empty -> collect the table
                            HandCardScore = mGame.GetCurrentScore;
                        else
                            HandCardScore = -1; //To avoid Selecting Jacks & 7 Diamonds as Highest Score index if table is empty.. unless its the only Choice its already selected
                    }
                    else
                    {
                        TableDeck = mGame.GetDeckCards();
                        if (TableDeck.Count == 1) // Handling the basra
                            if (mDeck.ElementAt(i).Value.number == TableDeck[0].number)
                                HandCardScore = mDeck.ElementAt(i).Value.number * 2 + 10;
                        HandCardScore = Game.hasSimilarCards(mDeck.ElementAt(i).Value.number, TableDeck);
                        if (mDeck.ElementAt(i).Value.number > 1 && mDeck.ElementAt(i).Value.number <= 10)
                            HandCardScore += Game.hasThatEquals(mDeck.ElementAt(i).Value.number, TableDeck) + TableDeck.Count > 0 ? 0 : 10;
                    }
                    if(HandCardScore>0)// if There is something to collect add my card score too
                        HighestScore += mDeck.ElementAt(i).Value.number;
                    if (HighestScore < HandCardScore) //get the card that brings highest score
                    {
                        HighestScore = HandCardScore;
                        highestScoreIndex = i;
                    }
                }


                ThrowCard(highestScoreIndex);
            }
        }
    }
    void Start()
    {
        isPC = true;
        base.Start();
    }
    public override void ThrowCard(int index)
    {
        StartCoroutine(playCard(index));
    }
    IEnumerator  playCard(int index)
    {

        yield return new WaitForSeconds(delay);
        int key = mDeck.ElementAt(index).Key;
        Card c = mDeck[key];
        _buttonCards[key].interactable = false;
        mDeck.Remove(key);
        // Throw it
        // not ai turn
        mGame.ThrowCard(c);
        IsMyTurn = false;

    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : PlayerBase
{
  
    [SerializeField]
   // private Text[] _textCards;
   
    public override bool IsMyTurn
    {
        get { return _isMyTurn; }
        set {
            _isMyTurn = value;
            if (_isMyTurn)
            {
                AudioManager.instance.PlayPlayersTurn();
                Handheld.Vibrate();
            }
        }
    }
    
    // Called from UI buttons for this time
    public override void ThrowCard(int index)
    {
        if (!_isMyTurn)
        {
            Debug.Log("Not my turn!");

            return;
        }
        // not my turn
        IsMyTurn = false;
        Card card = mDeck[index];
        // remove it
        mDeck.Remove(index);
        mGame._deck.removeCard(card);
        Debug.Log("Player Thrown card " + card.NumericName);
        // disable button
      //  _textCards[index].text = string.Empty;
        _buttonCards[index].interactable = false;
        // tell game 
       mGame.ThrowCard(card);
    }

    // Adds card to players deck
   }

﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public abstract class PlayerBase : MonoBehaviour
{
    public int Score 
    { 
        get { return _score; } 
        set 
        { 
            _score = value; 
            _textScore.text = "Score " + _score; 
        }
    }
    public int CardCount { get { return mDeck.Count; } }
    public Button[] _buttonCards;
    protected Dictionary<int, Card> mDeck = new Dictionary<int, Card>();
    protected Game mGame;
    public bool isPC;
    protected bool _isMyTurn;
    [SerializeField]
    protected Text _textScore;
    private int _score;
    
    public abstract bool IsMyTurn { get; set; }
    public abstract void ThrowCard(int index);

    public virtual void Start()
    {
        mGame = FindObjectOfType<Game>();
    }

    public virtual void AddCard(int index,Card card)
    {
        mDeck.Add(index, card);
        if (CardCount == 4)
            SetMyCards();
    }
    // Set UI for visual
    private void SetMyCards()
    {
        for (int i = 0; i < CardCount; i++)
        {
            //   _textCards[i].text = string.Format("{0}\n{1}", base.mDeck[i].cardSuit.ToString(), base.mDeck[i].NumericName);
            if (!isPC)
            {
                _buttonCards[i].GetComponent<Image>().sprite = mGame.Cards[mGame.getSpriteIndex(mDeck[i].suit, mDeck[i].number)];
            }
            _buttonCards[i].interactable = true;
        }
    }

}

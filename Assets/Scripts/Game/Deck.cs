﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Deck : MonoBehaviour
{
    readonly int _deckSize = 52;
    [SerializeField]
    private List<Card> _deck = new List<Card>();

    public int NumberOfCards
    {
        get { return _deck.Count; }
    }

    void Start()
    {
        CreateCardDeck();
    }

    private void CreateCardDeck()
    {
        for (int i = 0; i < 4; i++)
        {
            for (int j = 1; j <= _deckSize / 4; j++)
            {
                CardSuit suit = (CardSuit)i;
                Card card = new Card(suit, j);
                _deck.Add(card);
            }
        }
    }
    public void removeCard(Card card)
    {
        _deck.Remove(card);
    }
    public Card GetRandomCard()
    {
        Debug.Log("NumberOfCards "+ NumberOfCards);
        int i = Random.Range(0, NumberOfCards);
        Card c = _deck[i];
        _deck.RemoveAt(i);
        return c;
    }
    public Card GetRandomCardForTable()
    {
        Debug.Log("NumberOfCards " + NumberOfCards);
        int i = Random.Range(0, NumberOfCards);
        Card c = _deck[i];
        while((c.number==Card.JACK||c.number==7&&c.cardSuit==CardSuit.Diamonds)&&_deck.Count>4)
        {
            i = Random.Range(0, NumberOfCards);
            c = _deck[i];
        }
        _deck.RemoveAt(i);
        return c;
    }
}
